var exec = require('cordova/exec');

module.exports.coolMethod = function (arg0, success, error) {
  exec(success, error, 'LeonisaScan', 'coolMethod', [arg0]);
};

module.exports.add = function ( arg0, success, error ){
  exec(success, error, 'LeonisaScan', 'add', [arg0]);
}

module.exports.substract = function ( arg0, success, error ){
  exec(success, error, 'LeonisaScan', 'substract', [arg0]);
}