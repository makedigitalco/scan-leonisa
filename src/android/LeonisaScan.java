package cordova.plugin.leonisascan;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.SparseArray;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This class echoes a string called from JavaScript.
 */
public class LeonisaScan extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("coolMethod")) {
            String message = args.getString(0);
            this.coolMethod(message, callbackContext);
            return true;
        }else if (action.equals("add")) {
            this.add(args, callbackContext);
            return true;
        }else if (action.equals("substract")) {
            this.substract(args, callbackContext);
            return true;
        }     
        
        return false;
    }

    private void coolMethod(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
    
    private void substract(JSONArray args, CallbackContext callback) {
        if (args != null){
            try {
                String msj = "Hello World v3!";

                Context context = this.cordova.getActivity().getApplicationContext();
                int duration = Toast.LENGTH_LONG;

                String src = "https://cp.makedigital.com.co/cdn/demo.png";
                Bitmap myBitmap = this.getBitmapFromURL( src );

                BarcodeDetector detector = new BarcodeDetector.Builder(context)
                .setBarcodeFormats( Barcode.PDF417 | Barcode.DATA_MATRIX | Barcode.QR_CODE)
                .build();

                if(!detector.isOperational()){
                    msj = "Could not set up the detector!";
                }else{
                    msj = "Todo cool 3!";
                }

                Toast toast = Toast.makeText(context, msj, duration);
                toast.show();


                int p1 = Integer.parseInt( args.getJSONObject(0).getString("param1") );
                int p2 = Integer.parseInt( args.getJSONObject(0).getString("param2") );


                Frame frame = new Frame.Builder().setBitmap(myBitmap).build();
                SparseArray<Barcode> barcodes = detector.detect(frame);
                Barcode thisCode = barcodes.valueAt(0);


                // callback.success("" + ( p1 * p2 ) );
                callback.success("" + thisCode.rawValue );
                

            } catch (Exception ex) {
                callback.error("Algo esta mal " + ex);
            }
        }else{
            callback.error("Por favor no envies valores nulos");
        }
        
    }


    private void add(JSONArray args, CallbackContext callback) {
        if (args != null){
            try {
                int p1 = Integer.parseInt( args.getJSONObject(0).getString("param1") );
                int p2 = Integer.parseInt( args.getJSONObject(0).getString("param2") );

                callback.success("" + ( p1 + p2 ) );

            } catch (Exception ex) {
                callback.error("Algo esta mal " + ex);
            }
        }else{
            callback.error("Por favor no envies valores nulos");
        }
        
    }
}
